use bevy::gltf::Gltf;
use bevy::prelude::*;
use bevy::winit::WinitSettings;

use heron::prelude::*;
use heron::RigidBody;

use leafwing_input_manager::prelude::*;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(PhysicsPlugin::default())
        .add_plugin(InputManagerPlugin::<Action>::default())
        .insert_resource(WinitSettings::desktop_app())
        .insert_resource(Gravity::from(Vec3::new(0.0, -9.81, 0.0)))
        .add_startup_system(setup)
        .add_system(jump)
        .add_system(on_floor)
        .run();
}

#[derive(PhysicsLayer)]
enum Layer {
    Floor,
    Player,
    Enemy,
}

#[derive(Actionlike, PartialEq, Eq, Clone, Copy, Hash, Debug)]
enum Action {
    Jump,
    Forward,
    Left,
    Right,
    Backward,
}

#[derive(Component)]
struct Player;

#[derive(Default, Component)]
struct OnFloor {
    on_floor: bool,
}

fn on_floor(mut query: Query<&mut OnFloor>, mut events: EventReader<CollisionEvent>) {
    for event in events.iter() {
        let (entity0, entity1) = event.rigid_body_entities();
        let (layers0, layers1) = event.collision_layers();

        let floored = match (
            layers0.contains_group(Layer::Floor),
            layers1.contains_group(Layer::Floor),
        ) {
            (true, true) => continue,
            (false, false) => continue,
            (false, true) => entity0,
            (true, false) => entity1,
        };

        if let Ok(mut on_floor) = query.get_mut(floored) {
            on_floor.on_floor = event.is_started();
        }
    }
}

// Query for the `ActionState` component in your game logic systems!
fn jump(mut query: Query<(&ActionState<Action>, &OnFloor, &mut Velocity), With<Player>>) {
    let (action_state, on_floor, mut velocity) = query.single_mut();

    let mut x = 0.;

    if action_state.pressed(Action::Left) {
        x -= 1.;
    }

    if action_state.pressed(Action::Right) {
        x += 1.;
    }

    let mut z = 0.;

    if action_state.pressed(Action::Forward) {
        z -= 1.;
    }

    if action_state.pressed(Action::Backward) {
        z += 1.;
    }

    let y = if on_floor.on_floor && action_state.just_pressed(Action::Jump) {
        10.
    } else {
        0.
    };

    let target_velocity = Vec2::new(x, z).normalize_or_zero() * 10.;

    velocity.linear.x = target_velocity.x;
    velocity.linear.z = target_velocity.y;
    velocity.linear.y += y;
}

/// set up a simple 3D scene
fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>,
) {
    // light
    commands.spawn_bundle(PointLightBundle {
        point_light: PointLight {
            intensity: 15000.0,
            range: 1750.,
            shadows_enabled: true,
            ..default()
        },
        transform: Transform::from_xyz(0., 50., 0.),
        ..default()
    });
    // camera
    commands.spawn_bundle(Camera3dBundle {
        transform: Transform::from_xyz(0., 80., 100.).looking_at(Vec3::ZERO, Vec3::Y),
        ..default()
    });

    commands
        .spawn()
        .insert_bundle(PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Box {
                min_x: -50.,
                min_y: -0.5,
                min_z: -50.,

                max_x: 50.,
                max_y: 0.5,
                max_z: 50.,
            })),
            material: materials.add(StandardMaterial {
                base_color: Color::hex("ffffff").unwrap(),
                ..Default::default()
            }),
            ..Default::default()
        })
        .insert(RigidBody::Static)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec3::new(50., 0.5, 50.),
            border_radius: None,
        })
        .insert(CollisionLayers::new(Layer::Floor, Layer::Player))
        .insert(Transform::from_translation(Vec3::new(0., -0.5, 0.)));

    commands
        .spawn()
        .insert(Player)
        .insert_bundle(SceneBundle {
            scene: asset_server.load("art/player.glb#Scene0"),
            ..Default::default()
        })
        .insert(RigidBody::Dynamic)
        .insert(CollisionShape::Sphere { radius: 1.0 })
        .insert(RotationConstraints::lock())
        .insert(Velocity::default())
        .insert(CollisionLayers::new(Layer::Player, Layer::Floor))
        .insert(OnFloor::default())
        .insert_bundle(InputManagerBundle::<Action> {
            input_map: InputMap::new([
                (KeyCode::Space, Action::Jump),
                (KeyCode::A, Action::Left),
                (KeyCode::Left, Action::Left),
                (KeyCode::O, Action::Backward),
                (KeyCode::Down, Action::Backward),
                (KeyCode::Comma, Action::Forward),
                (KeyCode::Up, Action::Forward),
                (KeyCode::E, Action::Right),
                (KeyCode::Right, Action::Right),
            ]),
            ..Default::default()
        });
}
